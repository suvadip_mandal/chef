knife[:chef_repo_path] = "#{ENV['HOME']}/.chef"

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "chefuser"
client_key               "#{knife[:chef_repo_path]}/chefuser.pem"
validation_client_name   "orgchef-validator"
validation_key           "#{knife[:chef_repo_path]}/orgchef-validator.pem"
chef_server_url          "https://ec2-3-217-81-89.compute-1.amazonaws.com/organizations/orgchef"
cache_type               'BasicFile'
cache_options( :path => "#{knife[:chef_repo_path]}/checksums" )
cookbook_path            ["~/.chef/cookbooks"]
ssl_verify_mode      :verify_none
# path to environment files
environment_path "#{knife[:chef_repo_path]}/environments"

# Path to encrypted data bag and secret file
# knife[:secret_file]       = "/etc/chef/encrypted_data_bag_secret"

knife[:aws_access_key_id] = ENV['AWS_ACCESS_KEY_ID']
knife[:aws_secret_access_key] = ENV['AWS_SECRET_ACCESS_KEY']